import React from 'react';
import { Button, Spin } from 'antd';
import Cookies from 'js-cookie';
import { history } from 'umi';
import Workplace from '@/pages/workplace/index';
import Home from '@/pages/workplace/home';

class Index extends React.Component {
  componentWillMount() {
    // 根据用户权限判断展示首页
  }

  componentDidMount() {
    // 根据用户权限判断展示首页
  }

  render() {
    // 处理千人前面逻辑
    const SubContent = () => {
      // 项目自定义判断条件
      if (false) {
        return <Workplace />;
      }
      return <Home />;
    };

    return (
      <div size="large" spinning="false">
        {<SubContent />}
      </div>
    );
  }
}

export default Index;
