import React, { Fragment } from 'react';

import styles from '@/pages/index.css';
import ClassChild from './ClassChild';
import FunChild from './FunChild';
import ComChild from './ComChild';

class Home extends React.Component {
  state = {
    count: 0,
    name: 0,
  };

  render() {
    return (
      <Fragment>
        <div className={styles.normal}>
          <ClassChild count={this.state.count}></ClassChild>
          <FunChild count={this.state.count}></FunChild>
          <ComChild count={this.state.count}></ComChild>
          <button
            onClick={() => {
              this.setState(() => ({ count: this.state.count + 1 }));
            }}
          >
            加count
          </button>
          <button
            onClick={() => {
              this.setState(() => ({ name: this.state.count + 1 }));
            }}
          >
            加name
          </button>
          <ul className={styles.list}>
            <li>这是子应用首页</li>
          </ul>
          <div className={styles.welcome} />
        </div>
      </Fragment>
    );
  }
}

export default Home;
