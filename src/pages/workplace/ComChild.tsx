//引入PureComponent
import React, { Component } from 'react';
import { history } from 'umi';

//PureComponent，自动比较组件数据是否改变，注意只能比较一层，比如一个对象，对象中的属性改变，他不会重新渲染，只有对象改变，才重新渲染。
class ComChild extends Component {
  componentDidMount() {
    console.log('ComChild组件渲染');
  }

  onSelect = () => {
    //路由跳转
    history.push('/');
  };

  render() {
    console.log('ComChild组件渲染');
    return (
      <div>
        <div onClick={this.onSelect}>ComChild 点击</div>
      </div>
    );
  }
}

export default ComChild;
