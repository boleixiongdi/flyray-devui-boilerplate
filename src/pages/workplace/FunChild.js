//引入memo
import React, { Component, Fragment, memo } from 'react';
//用memo把hooks包裹即可
import { history } from 'umi';

const FunChild = memo(function FunChild(props) {
  const onSelect = ({ key }) => {
    //路由跳转
    console.log(key);
    history.push('/workplace');
  };

  console.log('FunChild组件渲染');
  return (
    <div>
      count:{props.count}
      <div onClick={onSelect}>FunChild 点击</div>
    </div>
  );
});

export default FunChild;
