import React from 'react';
import actions from '@/shared/actions';

class Workplace extends React.Component {
  constructor(props) {
    super(props);
    console.log('测试子应用与基座的通信');
    console.log(props);
  }

  communication = () => {
    //  子应用触发全局状态变化，其他应用可以监听到数据变化
    const sta = {
      menuTreeNo: '1',
    };
    actions.setGlobalState(sta);
  };

  render() {
    return (
      <div>
        <div>测试子应用与基座的通信</div>
        <button onClick={this.communication}>测试子应用与基座通信</button>
        <ul>
          <li>这是子应用首页</li>
          <li>
            <a href="https://umijs.org/guide/getting-started.html"></a>
          </li>
        </ul>
      </div>
    );
  }
}

export default Workplace;
