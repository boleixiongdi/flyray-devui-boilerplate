import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  // 浏览器兼容
  targets: {
    ie: 11,
  },
  // 开启路由动态加载
  // dynamicImport: {
  //   loading: '@/components/PageLoading',
  // },
  qiankun: {
    slave: {},
  },
  history: { type: 'hash' },
  dva: {
    // 开启dva-immer，用于代理currentState和nextState之间的改变，即当前状态修改副本
    immer: true,
    // 开启模块热加载(热更新)
    hmr: true,
  },
  antd: {},
  routes: [
    {
      //path不允许为空
      path: '/*',
      //严格路由匹配模式
      exact: true,
      component: '../layouts/index',
      routes: [
        {
          path: '/',
          component: '../pages/workplace/index',
          wrappers: ['@/routes/PrivateRoute.js'],
        },
        {
          path: '/workplace',
          component: '../pages/workplace/home',
          wrappers: ['@/routes/PrivateRoute.js'],
        },
        {
          path: '/externalLinks',
          component: '../pages/externalLinks/index',
          wrappers: ['@/routes/PrivateRoute.js'],
        },
        { path: '/*', component: '../pages/404', exact: true },
      ],
    },
    { path: '/noAuth', component: '../pages/noAuth/index' },
  ],
  // DefinePlugin 全局常量定义
  define: {},
});
